import React from "react";
import "./Order.css"


const Order = props => {
    return (
        <div className="Order">
            <p className="OrderTitle">Order Details</p>
            <p>{props.text}</p>
            {props.children}
            <p className="OrderTitle">Total price: {props.price} KGS</p>

        </div>
    )
};

export default Order;