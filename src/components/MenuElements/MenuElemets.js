import React from "react";
import "./MenuElements.css";

const MenuElements = props => {
    return(
     <div className="MenuElement" onClick={props.onClick}>
         <img src={props.image} alt=""/>
         <p>{props.name}</p>
         <p className="MenuElementPrice">Price: {props.price} KGS</p>
     </div>
    )
};

export default MenuElements;