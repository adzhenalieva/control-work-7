import React from "react";
import "./OrderDetails.css";

const OrderDetails = props => {
    return (
        <div className="OrderDetails">
            <span>{props.name}</span>
            <span>x{props.count}</span>
            <span>{props.price} KGS</span>
            <button className="BtnDelete" onClick={props.delete}>X</button>
        </div>
    )
};
export default OrderDetails;