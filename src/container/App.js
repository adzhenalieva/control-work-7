import React, {Component} from 'react';
import MenuElements from "../components/MenuElements/MenuElemets";
import Order from "../components/Order/Order";
import OrderDetails from "../components/OrderDetails/OrderDetails";

import './App.css';
import Hamburger from "../assets/hamburger.png";
import Cheeseburger from '../assets/cheeseburger.png';
import Fries from "../assets/fries.png";
import Coffee from '../assets/coffee.png';
import Tea from '../assets/tea.png';
import Cola from '../assets/cola.png';

class App extends Component {
    state = {
        totalPrice: 0,
        text: "Order is empty! Please, add some items!",
        products: [
            {name: 'Hamburger', count: 0, priceForOnePiece: 150, priceOfOrder: 0, image: Hamburger},
            {name: 'Cheeseburger', count: 0, priceForOnePiece: 170, priceOfOrder: 0, image: Cheeseburger},
            {name: 'Fries', count: 0, priceForOnePiece: 80, priceOfOrder: 0, image: Fries},
            {name: 'Coffee', count: 0, priceForOnePiece: 30, priceOfOrder: 0, image: Coffee},
            {name: 'Tea', count: 0, priceForOnePiece: 20, priceOfOrder: 0, image: Tea},
            {name: 'Cola', count: 0, priceForOnePiece: 30, priceOfOrder: 0, image: Cola}
        ]
    };

    addElement = (id) => {
        let products = [...this.state.products];
        let product = products[id];
        product.count++;
        let totalPrice = this.state.totalPrice + product.priceForOnePiece;
        product.priceOfOrder = product.priceOfOrder + product.priceForOnePiece;
        products[id] = product;
        let text = " ";
        this.setState({text, products, totalPrice});

    };

    removeElement = (id) => {
        let products = [...this.state.products];
        let product = products[id];
        product.count--;
        let totalPrice = this.state.totalPrice - product.priceForOnePiece;
        product.priceOfOrder = product.priceOfOrder - product.priceForOnePiece;
        products[id] = product;
        this.setState({totalPrice, products});
        if (totalPrice === 0) {
            let text = "Order is empty! Please, add some items!";
            this.setState({text});
        }
    };


    showOrderDetails = (products) => {
        let ordersArray = [];
        for (let i = 0; i < products.length; i++) {
            if (products[i].count > 0) {
                ordersArray.push(<OrderDetails key={i}
                                               name={products[i].name}
                                               price={products[i].priceOfOrder}
                                               count={products[i].count}
                                               delete={() => this.removeElement(i)}> </OrderDetails>);
            }
        }
        return ordersArray;
    };


    render() {
        return (
            <div className="App">
                <p className="Title">Menu</p>
                {this.state.products.map((item, id) =>
                    <MenuElements
                        key={id}
                        name={item.name}
                        price={item.priceForOnePiece}
                        image={item.image}
                        onClick={() => this.addElement(id)}
                    />
                )
                }
                <Order price={this.state.totalPrice} text={this.state.text}>
                    {this.showOrderDetails(this.state.products)}
                </Order>
            </div>
        );
    }
}

export default App;
